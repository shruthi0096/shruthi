package collectionsdemo;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class Demo1 {

	public static void main(String[] args) {

		Set<String> set=new TreeSet<String>();
		set.add("Shruthi");
		set.add("Shweta");
		set.add("Hema");
		set.add("Kusuma");
		set.add("Lavanya");
		set.add("Megs");
		set.add("Subbu");
		System.out.println(set);
		System.out.println(set.size());
		
		set.remove("Kusuma");
		
		System.out.println(set.size());

		
		System.out.println(set);
		
		Iterator<String> i= set.iterator();
		
		while(i.hasNext())
		{
			System.out.println(i.next());
		}

	}

}
