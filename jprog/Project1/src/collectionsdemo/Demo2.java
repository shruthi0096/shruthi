package collectionsdemo;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import jdbcdemos.Customer;
import p1.Read;

public class Demo2 {

	public static void main(String[] args) throws SQLException {

		List<String> set=new ArrayList<String>();
		set.add("Shruthi");
		set.add("Shweta");
		set.add("Hema");
		set.add("Kusuma");
		set.add("Lavanya");
		set.add("Subbu");
		set.add("Megs");
		
		System.out.println(set);
		System.out.println(set.size());
		
		set.remove(2);
		
		System.out.println(set.size());

		
		System.out.println(set);
		
		Iterator<String> i= set.iterator();
		
		while(i.hasNext())
		{
			System.out.println(i.next());
		}
//
		
		
//		System.out.println("Enter customer ID");
//		int cId = Read.sc.nextInt();
//		
//		System.out.println("Enter customer Name");
//		String cName =Read.sc.next();
//		
//		System.out.println("Enter customer Address");
//		String cAdd =Read.sc.next();
//		
//		System.out.println("Enter bill amount");
//		int bAmt = Read.sc.nextInt();
//		
//		Customer cust=new Customer();
//		cust.setCustomerId(cId);
//		cust.setCustomeNmae(cName);
//		cust.setCustomerAddress(cAdd);
//		cust.setBillAmount(bAmt);
//		
//		jdbcdemos.InsertCust.insertCustomerDetails(cust);
		
	}

}