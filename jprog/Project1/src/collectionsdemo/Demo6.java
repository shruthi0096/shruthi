package collectionsdemo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Demo6 {

	public static void main(String[] args) throws IOException {

		String readFile="data.txt";
		String writeFile="newData.txt";
		
		File fileR =new File(readFile);
		File fileW=new File(writeFile);
		
		FileInputStream rstream=new FileInputStream(fileR);
		FileOutputStream stream=new FileOutputStream(fileW);
		
		
		int i=0;
		while((i=rstream.read())!=-1)
		{
			stream.write((char)i);
		}
		
		System.out.println("Copied");
		rstream.close();
		stream.close();
		
//		String s="Welcome to javaTpoint.";    
//        byte b[]=s.getBytes();//converting string into byte array    
//        stream.write(b);    
//          
//        System.out.println("Success...");  
//		stream.close();
		
		
	}

}
