package collectionsdemo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Demo3 {

	public static void main(String[] args) {

		List<String> all=new ArrayList<String>();
		
		all.add("Anu");
		all.add("Harika");
		all.add("Rahul");
		all.add("Sai");
		all.add("Sherya");		
		all.add("Shruthi");
		int position=Collections.binarySearch(all, "Shruthi");
		
		Collections.sort(all);
		
		Iterator<String> i=all.iterator();
		
		while(i.hasNext())
		{
			System.out.println(i.next());
		}
		
		
		System.out.println(position);

	}

}
