package collectionsdemo;

//public class HelloGenericType<Z> {
//
//	public void display(Z s)
//	{
//		System.out.println(s);
//	}
//	
//	public void display(Z a,Y b)
//	{
//		
//	}
//	
//	public static void main(String[] args) {
//
//		HelloGenericType<Boolean> hello=new HelloGenericType<Boolean>();
//		hello.display(true);
//		
//		HelloGenericType<String> hello2=new HelloGenericType<String>();
//		hello2.display("Shruthi");
//		
//		
//		
//	}
//
//}


public class HelloGenericType<T> {

	
	public<T extends Number> double display(T a,T b)
	{
		return a.doubleValue()+b.doubleValue();
	}
	
	public static void main(String[] args) {

		HelloGenericType <Integer> hello=new HelloGenericType<Integer>();
		System.out.println(hello.display(10,15));
		
		
		HelloGenericType <Double> hello1=new HelloGenericType<Double>();
		System.out.println(hello1.display(105,150));
		
		
		
		
	}

}

