package iodemos;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class Demo4 {

	public static void main(String[] args) throws IOException, Throwable {

		ObjectInputStream stream=new ObjectInputStream(new BufferedInputStream(new FileInputStream("cust.txt")));
		
		Customer cust=(Customer)stream.readObject();
		
		System.out.println(cust);

	}

}
