package iodemos;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class Demo3 {

	public static void main(String[] args) throws IOException {

		Customer customer=new Customer(123,"Sam","Bangalore",65000);
		
		ObjectOutputStream stream=new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("cust.txt")));
		
		stream.writeObject(customer);
		stream.close();
		System.out.println("Customer Data Stored");

	}

}
