package exceptions;

public class NumberNegitiveException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NumberNegitiveException() {

	}
	
	public NumberNegitiveException(String message){
		super(message);
	}

}
