package exceptionDemo;

public class Demo7 {

	public static void main(String[] args) {

		StringBuffer buffer = new StringBuffer("PP"); //Thread Safe. Mutable.
		buffer.append("and QQ");
		System.out.println(buffer);
	
		StringBuilder build=new StringBuilder();	//Not Thread Safe. Mutable.
		build.append(" and QQ");
		System.out.println(build);
	
	}

}
