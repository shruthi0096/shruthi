package exceptionDemo;

public class Demo5 {

	public static void main(String[] args) {

		String marks="98";
		System.out.println(marks);
		System.out.println(Integer.parseInt(marks)+10);
		
		//primitive-->object(boxing)
		int num=12;
		Integer num1=num;				//Auto boxing
		Integer n=new Integer(num);		//Boxing
		
		//object-->primitive(unboxing)
		Integer scores=100;
		int lastScore=scores;			//auto unboxing
		int i=Integer.valueOf(scores);	//unboxing
		
		
	}

}
