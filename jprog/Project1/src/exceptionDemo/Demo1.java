package exceptionDemo;

import exceptions.NumberNegitiveException;
import p1.Read;

public class Demo1 {

	int num1,num2,result;
	
	public void display() throws NumberNegitiveException
	{
		try
		{
			System.out.println("Enter 1st Number: ");
			num1=Read.sc.nextInt();
		
			if(num1<0)
				try 
			{
					throw new NumberNegitiveException("Enter Positive Numbers");
			} 
			catch (NumberNegitiveException e)
			{
					// TODO Auto-generated catch block
					e.printStackTrace();
			}
			 
		
			System.out.println("Enter the 2nd Number: ");
			num2=Read.sc.nextInt();
		
			result=num1/num2;
		} 
		
		catch (Exception e) 
		{		
			System.out.println("Please Enter Proper Values.");
		}
		
		System.out.println("The result is: "+result);
	}
	
	public static void main(String[] args) {

		Demo1 d1=new Demo1();
		d1.display();
		System.out.println("Thanks for using my program.");
	}
	
	

}
