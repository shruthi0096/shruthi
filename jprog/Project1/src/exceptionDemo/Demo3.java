package exceptionDemo;

public class Demo3 {

	String name="Om";
	public void display() throws InterruptedException
	{
		System.out.println("The length of name is : "+name.length());
		Thread.sleep(2000);
		System.out.println("The Name is: "+name);
	}
	
	public static void main(String[] args) throws InterruptedException {
		
		Demo3 d=new Demo3();
		d.display();
		System.out.println("Thank you for using my program.");
	}
}
