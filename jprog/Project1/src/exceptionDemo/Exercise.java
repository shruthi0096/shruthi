package exceptionDemo;

public class Exercise {

	public static void main(String[] args) {

		String str= "The quick brown fox jumps over the lazy dog";
		
		System.out.println("The character at the 12th index: "+str. charAt(12));
		System.out.println("\nCheck whether the String contains the word �is� :"+str.contains("is"));
		str=str.concat(" and killed it");
		System.out.println("\nAdd the string �and killed it� to the existing string: "+str);
		System.out.println("\nCheck whether the String ends with the word �dogs� : "+str.endsWith("dogs"));
		System.out.println("\nCheck whether the String is equal to 'The quick brown Fox jumps over the lazy Dog': "+str.equals("The quick brown Fox jumps over the lazy Dog"));
		System.out.println("\nCheck whether the String is equal to �THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG�: "+str.equals("THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG"));
		System.out.println("\nFind the length of the String:"+str.length());
		System.out.println("\nCheck whether the String matches to �The quick brown Fox jumps over the lazy Dog�: "+str.compareTo("The quick brown Fox jumps over the lazy Dog"));
		System.out.println("\nReplace the word �The� with the word �A�: "+str.replaceAll("The","A"));
		System.out.println("\nSplit the above string into two such that two animal names do not come together: "+str.split("fox"));
		System.out.println("Print the animal names alone separately from the above string: ");
	
	}

}
