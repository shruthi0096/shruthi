package exceptionDemo;

public class Demo2a {

	public static void main(String[] args) {
		
		try {
			int marks[]=new int[-5];
			System.out.println(marks[5]);
			System.out.println("Thank you for using my program.");
		} 
		catch (ArrayIndexOutOfBoundsException e) {
			
			System.out.println("Array Index Out Of Bounds.");
		}

		catch (NegativeArraySizeException e) {

			System.out.println("Array index cannot be negitive.");
		}
		
		catch (Exception e) {

			System.out.println("Please check for errors.");
		}
		
		finally {
			System.out.println("DONE.");
		}
	
		System.out.println("Thank You.");
	}
	
	

}
