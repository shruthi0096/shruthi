package p1;

public class Day1Demo4 {

	public static void main(String[] args) {
		
		System.out.println("Enter a number in Decimal:");
		int n=Read.sc.nextInt();
		System.out.println(Integer.toBinaryString(n));
		System.out.println(Integer.toOctalString(n));
		System.out.println(Integer.toHexString(n));

	}

}
