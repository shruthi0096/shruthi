package p1;

public class Int_Array {
	
	int[] arr;
	int size;
	
	
	public Int_Array() {
		super();
		arr = new int[10];

	}


	public Int_Array(int n) {
		super();
		arr = new int[n];
	}

	public Int_Array(int[] n) {
		super();
		arr=n;
	}

	public Int_Array(Int_Array ob) {
		this.arr=ob.arr;
	}
	
	public void display() {
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i]);
		}
		System.out.println();
	}
	
	public void display(int flag) {
		for (int j : arr) {
			System.out.println(j);

		}

	}
	
	

}
