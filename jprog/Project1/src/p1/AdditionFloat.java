package p1;

public class AdditionFloat {

	public static void main(String[] args) {

		if(args.length !=2)
		{
			System.out.println("No. arguments mismatched.Please enter two values");
			return;
		}
		
	 
		float a=Float.parseFloat(args[0]);
		float b=Float.parseFloat(args[1]);
		float c= a+b;

		System.out.println("a="+a+ " b="+b+" sum="+c);

	}

}
