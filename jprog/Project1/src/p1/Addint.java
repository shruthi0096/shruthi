package p1;


public class Addint {
	
		int number1,num2,num3;
		
		public Addint(int number1) {
			super();
			this.number1 = number1;
			System.out.println("In 1 arg constructor");

		}

		public Addint() {
			super();
			System.out.println("In 0 arg constructor");
		}

		public Addint(int number1, int num2) {
			super();
			this.number1 = number1;
			this.num2 = num2;
			System.out.println("In 2 arg constructor");

		}

		public void setData(int num1,int num2)
		{
			this.number1=num1;
			this.num2=num2;
		}

		public void calculate()
		{
			this.num3=number1+num2;
	
		}
		
		public void display()
		{
				System.out.println("num1="+number1+" num2="+num2+" num3="+num3);
		}
	

}
