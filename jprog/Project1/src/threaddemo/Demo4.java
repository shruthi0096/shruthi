package threaddemo;

public class Demo4 {
	
	int j=100;

	private class A
	{
		int i=90;
		
		public void display()
		{
			System.out.println("The value of i is: "+i);
			System.out.println("The value of j is: "+j);
		}
	}
	
	public static void main(String[] args) {
		
		Demo4 d=new Demo4();
		Demo4.A a=d.new A();
		a.display();
	}
	
}

//public class ChangePassword {
//	
//	int j=100;
//
//	private class EncryptPassword
//	{
//		int i=90;
//		
//		public void display()
//		{
//			System.out.println("The value of i is: "+i);
//			System.out.println("The value of j is: "+j);
//		}
//	}
//	
//	public static void main(String[] args) {
//		
//		ChangePassword d=new ChangePassword();
//		ChangePassword.EncryptPassword a=d.new EncryptPassword();
//		a.display();
//	}
//	
//}