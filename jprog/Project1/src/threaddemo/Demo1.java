package threaddemo;

public class Demo1 extends Thread{

	Thread t1,t2;
	
	public Demo1()
	{
		t1=new Thread(this);
		t1.start();
		t2=new Thread(this);
		t2.start();
	}
	
	
	@Override
	public void run()
	{
		System.out.println("Run Called "+Thread.currentThread().getName());
	}
	
	public static void main(String[] args) {

		Demo1 d= new Demo1();
		System.out.println("Main Called "+Thread.currentThread().getName());
		
		
	}

}



//public class Demo1 implements Runnable 
//{
//
//	Thread t1,t2;
//	
//	public Demo1()
//	{
//		t1=new Thread(this);
//		t1.start();
//		t2=new Thread(this);
//		t2.start();
//	}
//	
//	
//	@Override
//	public void run()
//	{
//		System.out.println("Run Called "+Thread.currentThread().getName());
//	}
//	
//	public static void main(String[] args) {
//
//		Demo1 d= new Demo1();
//		System.out.println("Main Called "+Thread.currentThread().getName());
//		
//		
//	}
//
//}

