package threaddemo;

//public class Demo2 extends Thread{
//
//	@Override
//	public void run() 
//	{
//		System.out.println("RUN Called by: "+Thread.currentThread().getName());
//	}
//	
//	public Demo2(String x) 
//	{
//		super(x);
//		start();	
//	}
//	
//	public static void main(String[] args) {
//		
//		new Demo2("ITPL1");
//		new Demo2("ITPL2");
//		new Demo2("ITPL3");
//		new Demo2("ITPL4");
//		new Demo2("ITPL5");
//		System.out.println("RUN Called by: "+Thread.currentThread().getName());
//
//
//	}
//
//}


public class Demo2 extends Thread{

	@Override
	public void run() 
	{
		System.out.println("RUN Called by: "+Thread.currentThread().getName());
	}
	
	public Demo2(int x) 
	{
		super("ITPL"+x);
		start();	
	}
	
	public static void main(String[] args) {
		
		for (int i = 0; i <=5; i++) 
		{
			new Demo2(i);
		}
		
		System.out.println("RUN Called by: "+Thread.currentThread().getName());


	}

}