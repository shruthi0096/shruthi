package threaddemo;

public class Demo5 {
	
	interface Radio
	{
		public void play();
	}
	
	public static void main(String[] args) {
		
		Radio r =new Radio()
				{

					@Override
					public void play() {
							System.out.println("Radio Playing.");						
					}
			
				};
				
				r.play();
	}

}
