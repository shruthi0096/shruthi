package threaddemo;

public class Demo3 extends Thread {
	
	public static synchronized void display(String threadName,String message1,String message2)
	{
		System.out.println("Message by: "+threadName);
		System.out.println("Messgae 1 is: "+message1);
		
		try
		{
			Thread.sleep(1000);
		}
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
		System.out.println("Message 2 is: "+message2);
	}

	public Demo3(int i)
	{
		super(""+i);
		start();
	}
	
	@Override
	public void run() 
	{
		display(Thread.currentThread().getName(), "Hi", "Bye");
		System.out.println("Run Called By :" +Thread.currentThread().getName());
	}
	
	public static void main(String[] args) {

		for (int i = 0; i <=4; i++) 
		{
			new Demo3(i);
		}
		
		System.out.println("RUN Called by: "+Thread.currentThread().getName());
	}

}
