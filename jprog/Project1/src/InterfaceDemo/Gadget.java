package InterfaceDemo;

public abstract class Gadget {
	
	int ram=6;

}

interface Radio{
	int freq=0;
	void play();
}

interface Mradio extends Radio{
	void pause();
}

interface Apps{
	
}

class Mobile extends Gadget implements Apps,Mradio{

	
	
	@Override
	public void play() {
		System.out.println("Ram = "+ram);
		System.out.println("Frequency = "+freq);
		System.out.println("Play Radio");
	}

	@Override
	public void pause() {
		System.out.println("Pause Radio");
		
	}
	
}

class Execute
{
	public static void main(String[] args) {
		
		Mobile m=new Mobile();
		m.play();
		m.pause();
	}
}