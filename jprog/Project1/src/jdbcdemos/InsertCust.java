package jdbcdemos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.javatraining.dbconn.DBConfig;

import p1.Read;
public class InsertCust {

	public static void main(String[] args) throws SQLException {
		System.out.println("Enter customer ID");
		int cId = Read.sc.nextInt();
		
		System.out.println("Enter customer Name");
		String cName =Read.sc.next();
		
		System.out.println("Enter customer Address");
		String cAdd =Read.sc.next();
		
		System.out.println("Enter bill amount");
		int bAmt = Read.sc.nextInt();
		
		
		PreparedStatement statement = insertCustomerDetails2(cId, cName, cAdd, bAmt);
		
		int rows = statement.executeUpdate();
		
		System.out.println(rows + " Updated");
		

	}

	public static PreparedStatement insertCustomerDetails2(int cId, String cName, String cAdd, int bAmt)
			throws SQLException 
	{
		Connection conn = DBConfig.getConnection();
		PreparedStatement statement = conn.prepareStatement("insert into customer values(?,?,?,?)");
		statement.setInt(1, cId);
		statement.setString(2, cName);
		statement.setString(3, cAdd);
		statement.setInt(4, bAmt);
		
		int rows = statement.executeUpdate();
		System.out.println(rows + " Updated");

		return statement;

	}

	
	public static PreparedStatement insertCustomerDetails(Customer customer)
			throws SQLException 
	{
		Connection conn = DBConfig.getConnection();
		PreparedStatement statement = conn.prepareStatement("insert into customer values(?,?,?,?)");
		statement.setInt(1, customer.getCustomerId());
		statement.setString(2, customer.getCustomeNmae());
		statement.setString(3, customer.getCustomerAddress());
		statement.setInt(4, customer.getBillAmount());
		
		int rows = statement.executeUpdate();
		System.out.println(rows + " Updated");
		
		return statement;

	}
}
