package jdbcdemos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.javatraining.dbconn.DBConfig;

import p1.Read;

public class Demo4 {

	public static void main(String[] args) throws SQLException {

		Connection conn=DBConfig.getConnection();
		
		System.out.println("Enter customer ID");
		int cId = Read.sc.nextInt();
		
		String query = "select customerId from customer where customerId=? ";
		
		
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setInt(1, cId);
		
		ResultSet resultSet = ps.executeQuery();

		if(resultSet.next())
		{
			System.out.println("Enter customer Address");
			String cAdd =Read.sc.next();
			
			System.out.println("Enter bill amount");
			int bAmt = Read.sc.nextInt();
			
			String query1 = "update customer set customerAddress = ? , billAmount=? where customerId = ?";
			
			 PreparedStatement preparedStmt = conn.prepareStatement(query1);
		      preparedStmt.setString(1,cAdd );
		      preparedStmt.setInt(2, bAmt);
		      preparedStmt.setInt(3, cId);
			
		      int rows = preparedStmt.executeUpdate();
		      System.out.println(rows + " Updated");
		      System.out.println("Congradulations,"+cId+" Updated with new adress: "+cAdd+" and new Bill Amount: "+bAmt);

		      
		      conn.close();
		}
		else
		{
		System.out.println("CustomerId :"+cId+" does not Exist.");
		}
	
	}

}
