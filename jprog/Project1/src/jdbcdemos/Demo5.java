package jdbcdemos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.javatraining.dbconn.DBConfig;

import p1.Read;

public class Demo5 {

	public static void main(String[] args) throws SQLException {


		Connection conn=DBConfig.getConnection();
		
		System.out.println("Enter user name");
		String username = Read.sc.next();
		//String username = Read.sc.next();//When name has spaces

		
		System.out.println("Enter user password");
		String userpass = Read.sc.next();
		
		String query = "select * from UserTable where userName= ? and password= ? ";
		PreparedStatement preparedStatement=conn.prepareStatement(query);
		preparedStatement.setString(1, username);
		preparedStatement.setString(2, userpass);
		
		ResultSet rs=preparedStatement.executeQuery();
		
		if(rs.next())
		{
			System.out.println("Valid user.");
		}
		else
		{
			System.out.println("Invalid User.");
		}
	
		conn.close();
		rs.close();
		preparedStatement.close();
	}

}
