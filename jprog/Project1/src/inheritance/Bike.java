package inheritance;

public abstract class Bike extends Vehicle {

	@Override
	public void start() {
		System.out.println("\nBike Started.");
	}

	public abstract void kickstart();
}
