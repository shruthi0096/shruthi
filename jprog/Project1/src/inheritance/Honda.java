package inheritance;

public class Honda extends Bike {

	@Override
	public void kickstart() {
		System.out.println("Kick Start Honda Bike.");
	}

	@Override
	public void stop() {
		System.out.println("Stopped.");		
		
	}

}
