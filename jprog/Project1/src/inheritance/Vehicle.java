package inheritance;

abstract class Vehicle {
	
	public Vehicle() 
	{
		System.out.println("\nVehicle Constructor.");	
	}
	
	String color;
	int noOfWheels;
	public abstract void start();
	public abstract void stop();

}
