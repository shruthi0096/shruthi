package inheritance;

public class Bus extends Vehicle {
	
	String busType;
	
	public Bus() 
	{
		System.out.println("\nBus Constructor.");	
	}
	
	public void start()
	{
		System.out.println("\nBus Started.");

	}
	
	void showDetails()
	{
		color="Red";
		noOfWheels=6;
		busType="Volvo";
		
		System.out.println("Bus Color is:"+color);
		System.out.println("No of Wheels Bus has is:"+noOfWheels);
		System.out.println("Bus Type is:"+busType);
	}

	@Override
	public void stop() {
		System.out.println("Stopped.");		
		
	}

}
