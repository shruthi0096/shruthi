package p2;

public class employee_bean {

	private String name;
	private int age;
	private int salary;
	private int i_tax;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	public int getI_tax() {
		return i_tax;
	}
	public void setI_tax(int i_tax) {
		this.i_tax = i_tax;
	}
	
	
}
