package p2;

public class Day2One {
	
	private int a1=10;
	int a2=20;
	protected  int a3=30;
	public int a4=40;
	
	private void f1() {
		System.out.println("im in f1");
	}
	
	void f2() {
		System.out.println("im in f2");
	}
	
	protected void f3() {
		System.out.println("im in f3");
	}
	
	public void f4() {
		System.out.println("im in f4");
		System.out.println(a1);
		f1();
	}
	
	public Day2One() {
		System.out.println("im in constructor");
	}

}
