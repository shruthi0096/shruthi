package p2;

public class Emp_Bo {
	
	public void calculate_Tax(employee_bean ob) {
		
		int salary=ob.getSalary();
		int tax=0;
		
		if(salary<300000) {
			tax=0;
		}
		else if(salary<=500000) {
			tax=tax+(int)((salary-300000)*0.05);
		}
		else if(salary<=100000) {
			tax=tax+(int)((salary-500000)*0.2+10000);
		}
		else {
			tax=tax+(int)((salary-1000000)*0.3+110000);
		}
	}

}
