package com.javatraining.customer.client;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.javatraining.customer.model.Customer;

public class Client {

	public static void main(String[] args) {

//		Customer customer=new Customer();
//		customer.setCustomeName("Shruthi");
//		System.out.println("Customer Name is: "+customer.getCustomeNmae());
		
		Resource resource=new ClassPathResource("beans.xml");
		BeanFactory factory=new XmlBeanFactory(resource);
		
		Customer customer=(Customer)factory.getBean("cust2");
		//customer.setCustomeName("Shruthi");
		
		System.out.println(customer);
		
	}

}
