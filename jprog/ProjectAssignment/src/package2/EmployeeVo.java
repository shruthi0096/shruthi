package package2;

public class EmployeeVo {
	
	private String empName;
	private int empId;
	private int salary;
	private int i_tax;
	
	
	
	public EmployeeVo(String empName, int empId, int salary, int i_tax) {
		super();
		this.empName = empName;
		this.empId = empId;
		this.salary = salary;
		this.i_tax = i_tax;
	}
	
	
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	public int getI_tax() {
		return i_tax;
	}
	public void setI_tax(int i_tax) {
		this.i_tax = i_tax;
	}
	@Override
	public String toString() {
		return "EmployeeVo [empName=" + empName + ", empId=" + empId + ", salary=" + salary + ", i_tax=" + i_tax + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + empId;
		result = prime * result + ((empName == null) ? 0 : empName.hashCode());
		result = prime * result + i_tax;
		result = prime * result + salary;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmployeeVo other = (EmployeeVo) obj;
		if (empId != other.empId)
			return false;
		if (empName == null) {
			if (other.empName != null)
				return false;
		} else if (!empName.equals(other.empName))
			return false;
		if (i_tax != other.i_tax)
			return false;
		if (salary != other.salary)
			return false;
		return true;
	}
	

	
	
	
	
}
