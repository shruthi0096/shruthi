public class Addition
{
	public static void main(String[] args)
	{
		if(args.length !=2)
		{
			System.out.println("No. arguments mismatched.Please enter two values");
			return;
		}
		int a=Integer.parseInt(args[0]);
		int b=Integer.parseInt(args[1]);
		int c= a+b;

		System.out.println("a="+a+ " b="+b+" sum="+c);
	}
}		