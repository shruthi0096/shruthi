package com.javatraining.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import com.javatraining.customer.model.BankAccount;
import com.javatraining.customer.model.Customer;

@Configuration
public class AppConfig {
	


	@Bean
	@Scope("prototype")
	public Customer getCustomerObject()
	{
		BankAccount account=this.getBankAccountObject();
		return new Customer(account);
		
	}

	@Bean
	@Scope("prototype")
	public BankAccount getBankAccountObject()
	{
		return new BankAccount();
	}

}
