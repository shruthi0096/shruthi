package com.javatraining.customer.client;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.javatraining.config.AppConfig;
import com.javatraining.customer.model.BankAccount;
import com.javatraining.customer.model.Customer;

public class Client {

	public static void main(String[] args) 
	{

		ApplicationContext context= new AnnotationConfigApplicationContext(AppConfig.class);
		
		Customer customer1=context.getBean(Customer.class);
		Customer customer2=context.getBean(Customer.class);

		customer1.setCustomerName("Shruthi");
		customer1.setCustomerId(12);
		customer1.setCustomerAddress("Bangalore");
		customer1.setBillAmount(1230);
		
		customer2.setCustomerName("Dimple");
		customer2.setCustomerId(22);
		customer2.setCustomerAddress("Bangalore");
		customer2.setBillAmount(4560);
		
		
		BankAccount bankAccount=context.getBean(BankAccount.class);
		bankAccount.setAccountNumber("14523");
		bankAccount.setBalance(2580);
		customer1.setBankAccount(bankAccount);
		

		BankAccount bankAccount1=context.getBean(BankAccount.class);
		bankAccount1.setAccountNumber("10002");
		bankAccount1.setBalance(2110);
		customer2.setBankAccount(bankAccount1);

		
		System.out.println(customer1);
		System.out.println(customer2);
		
		
	}

}
