package com;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.javatraining.customer.dao.CustomerDAO;
import com.javatraining.customer.impl.CustomerDAOImpl;
import com.javatraining.customer.model.Customer;

/**
 * Servlet implementation class CustomerInfo
 */
public class CustomerInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CustomerInfo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
    
    
    
    
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int id=Integer.parseInt(request.getParameter("customerId"));
		String name=request.getParameter("customerName");
		String addres=request.getParameter("customerAddress");
		int bill=Integer.parseInt(request.getParameter("billAmount"));
		
		Customer customer=new Customer(id,name,addres,bill);
		CustomerDAO customerDAO=new CustomerDAOImpl();
		int result=customerDAO.insertCustomer(customer);

		response.getWriter().println("<h1>User id is- "+id+"</h1>");
		response.getWriter().println("<h1>User name is- "+name+"</h1>");
		response.getWriter().println("<h1>User address is- "+addres+"</h1>");
		response.getWriter().println("<h1>User id is- "+bill+"</h1>");
	
	
		
		
		response.getWriter().println(result+" rows stored.");


		//testing delete customer
//		int customerId=Integer.parseInt(request.getParameter("customerId"));
//		CustomerDAO dao=new CustomerDAOImpl();
//		int rows=dao.deleteCustomer(customerId);
//		System.out.println(rows+" deleted.");

	
	}

}
