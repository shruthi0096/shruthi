package com;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Welcome
 */
public class Welcome extends HttpServlet {
	private static final long serialVersionUID = 1L;
       int counter=0;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Welcome() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		counter++;
		
	    response.setContentType("text/html");  

		String username=request.getParameter("uname");
		String userpass=request.getParameter("upass");
		
		boolean alreadyvist=false;
		Cookie allCookies[]=request.getCookies();
		
		if(allCookies!=null)
		{
			for(Cookie c: allCookies)
			{
				if(c.getName().equals(username) && c.getValue().equals(userpass))
				{
					alreadyvist=true;
					break;
				}
				else 
				{
					if(c.getName().equals(username) && c.getValue()!=userpass)
					{
						alreadyvist=false;
						break;
					}
				}
			}
			
		}
		
//		if(username.equalsIgnoreCase("ram"))
//		{
			HttpSession session=request.getSession();
			session.setAttribute("n1", username);
			
//			RequestDispatcher dispatcher=request.getRequestDispatcher("Enter");
//			dispatcher.include(request, response);
		
			if(alreadyvist)
			{
				response.getWriter().println("<h3>WELCOME to my Web Page- "+username+" You have already visited</h3>");
				response.getWriter().println("<font color='green'>Welcome user number : "+counter);
				response.getWriter().println("<br><a href='Enter.html'>Enter</a>");
			}
			else
			{
				
						response.getWriter().println("<h1>WELCOME to my Web Page- "+username+"You are a New visitor</h1>");
						response.getWriter().println("<font color='green'>Welcome user number : "+counter);
						response.getWriter().println("<br><a href='Enter.html'>Enter</a>");
						Cookie cookie=new Cookie(username, userpass);
						response.addCookie(cookie);
					
				
				
				
			}
			
			
			
//		}
//		else
//		{
//			response.sendRedirect("Home.html");
//		}

		
	}

}
