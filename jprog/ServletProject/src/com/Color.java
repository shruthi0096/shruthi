package com;

import java.io.IOException;
import java.util.Iterator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Color
 */
public class Color extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Color() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String name=request.getParameter("uname");
		String color[]=request.getParameterValues("color");
		
		if(color!=null)
		{
			for(String c: color)
			{
				request.getParameter(c);
				response.getWriter().println("<font color="+c+">Hello "+name+"</font><br>");

			}
			
			RequestDispatcher dispatcher=request.getRequestDispatcher("NameColor.html");
			dispatcher.include(request, response);
		}
		else
		{
			response.getWriter().println("No Color selected<br>");
			RequestDispatcher dispatcher=request.getRequestDispatcher("NameColor.html");
			dispatcher.include(request, response);

		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
